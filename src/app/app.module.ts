import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import {FormsModule} from "@angular/forms";
import { TaskedComponent } from './tasked/tasked.component';

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    TaskedComponent
  ],
    imports: [
        BrowserModule,
        FormsModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
