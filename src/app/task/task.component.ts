import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']

})
export class TaskComponent{
  @Input() task = '';
  tasked = [{task: 'buy milk'}, {task: 'play game'}, {task: 'do homework'}];

  addTask(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.task = target.value;
  }

  addNewTask(event: Event) {
    event.preventDefault();
    this.tasked.push({task: this.task});
  }

  onDeleteTask(index: number){
    this.tasked.splice(index, 1);
    console.log('deleted', index)
  }

  changeTask(index: number, newTask : string) {
      this.tasked[index].task = newTask;
      console.log(this.tasked[index].task )
  }

  formIsEmpty() {
    return this.task === '';
  }
}
