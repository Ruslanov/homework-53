import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-tasked',
  templateUrl: './tasked.component.html',
  styleUrls: ['./tasked.component.css']
})
export class TaskedComponent  {
  @Input() tas = '';
  @Input() form = false;
  @Input() checkBox = false;
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();

  onDelete() {
    this.delete.emit();
  }

  onTaskChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }

  onFocus(){
  this.form = true;
  }

  focusOut() {
    this.form = false;
  }

  className() {
    return this.form ? 'focus' : 'noFocus'
  }

  check() {
    return this.checkBox ? 'done' : 'noDone'
  }
}
